#!/usr/bin/env python
'''
Created on 02/03/2020

@author: klay saunders

Get isomiR counts for a sample bam.

Requires isomir_mature_df.csv. 
isomir_mature_df contains microRNA names, locus and sequence for hg19 reference. 

Isomirs defined by differing start and/or end position. This script does not 
identify isomirs with differing sequence modifications.

'''

from argparse import ArgumentParser
from collections import Counter
from os import path
import pandas as pd
import HTSeq
import sys
import regex as re


#Argument parser
def handle_args():
    parser = ArgumentParser(description='Get isomiR counts from bam')
    parser.add_argument("--outdir", required=True, help="Directory to put count file and other stuff into.")
    parser.add_argument("--mature_isomir_file", required=True, help="Path to the isomir_mature_df")
    parser.add_argument("bam", help= "Bam file path")
    return parser.parse_args()

def iv_format_with_strand(iv):
    return "%s:%d-%d_%s" % (iv.chrom, iv.start, iv.end, iv.strand)

    
def name_from_file_name(file_name):
    '''Gets file name with removing extension and directory'''
    return path.splitext(path.basename(file_name))[0]


if __name__ == '__main__':
    
    #Parameters
    args = handle_args()
    
    outdir = args.outdir 
    bam_file = args.bam
    mature_file = args.mature_isomir_file
    
    #Set up and import isomir genomic locus
    mature_mir_df = pd.read_csv(mature_file, index_col=0)
    print(mature_mir_df.head())
    
    ### Process bam into a csv of isomiR counts ###
    sample_name = name_from_file_name(bam_file)
    print("Processing %s" % sample_name)
       
    bam_reader = HTSeq.BAM_Reader(bam_file)
    
    isomir_ivs_and_names = {}
    isomir_counter = Counter()
    wrong_strand_alignments = 0
    processed_alignments = 0
    for mir_name, iv in mature_mir_df['iv'].iteritems():
        chrom, start, end, strand = re.split('\:\[|,|\)\/', iv)
        
        for aln in bam_reader[HTSeq.GenomicInterval(chrom, int(start), int(end), strand)]:
            
            if aln.iv.strand != strand: #Sequencing was strand-specific so only consider alignments on correct strand
                wrong_strand_alignments += 1
                continue
                               
            iv_str = iv_format_with_strand(aln.iv)
            if not (iv_str in isomir_ivs_and_names): 
                isomir_ivs_and_names[iv_str] = mir_name
            isomir_counter[iv_str] += 1
            processed_alignments += 1
            
    isomir_df = pd.DataFrame({'mir_name': isomir_ivs_and_names, 
                              'count': isomir_counter}, columns=['mir_name', 'count'])
    print(isomir_df.head())
    isomir_df.index.name = 'iv'
    isomir_df.to_csv(path.join(outdir, "%s_isomir_counts.tsv" % sample_name), sep='\t')
    
    print("Also producing count file for miRs")
    mir_df = isomir_df.groupby('mir_name').sum()
    mir_df.to_csv(path.join(outdir, "%s_mir_counts.tsv" % sample_name), sep='\t')
    
    print("Alignments on wrong strand: %d (%.1f%%)" % (wrong_strand_alignments, 100 * wrong_strand_alignments / (wrong_strand_alignments + processed_alignments)))
    print("Processed alignments: %d" % processed_alignments)
    
