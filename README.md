INTRO
-----

This is for the paper: **Insufficiently complex unique-molecular identifiers (UMIs) distort small RNA sequencing** by **Klay Saunders. et al**. 

[Online Release Link](https://www.nature.com/articles/s41598-020-71323-0)


NOTES
-----
Requres a local python 3+ install. This is a stand-alone script and requires no other dependencies. 

CLONE REPOSITORY
----------------

Clone the repository for this project.

```
#!bash

git clone https://USER@bitbucket.org/sacgf/saunders_umi_2020.git
```

RUNNING
-------


```
#!bash

cd saunders_umi_2020

saunders_umi_paper_isomir_counting.py --outdir . --mature_isomir_file isomir_mature_df.csv input_bam_file
```
